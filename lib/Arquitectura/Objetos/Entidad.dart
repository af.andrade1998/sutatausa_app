// ignore_for_file: file_names

class Entidad {
  String nombre = '',
      descripcion = '',
      urlimgportada = '',
      urlimgperfil = '',
      direccion = '',
      urlmaps = '',
      urlcoleccionproductos = '',
      // Cadena de texto que tenga días seguido de hora de entrada y salida
      // Exp: 'L/8:0-20:0,M/8:0-20:0,MI/8:0-20:0,J/8:0-20:0,V/8:0-20:0,S/8:0-12:0'
      // Esta cadena se leeera y saldrá si esta disponible o no el establecimiento
      horarioatencion = '',
      paginaweb = '',
      domicilio = '',
      idfirebase = '';
  // Lista de cadenas de texto a modo de tags apra ver en que paginas de
  // categorias y subcategorias se encuentran
  List<String> listacategorias = [];
  // Lista de cadenas de texto de los numeros de contacto
  // Las cadenas de texto tendran indicativo de si tienen whatsapp disponible
  // Exp: 'W-3224143119'
  List<String> numerosdecontacto = [];
  // Lista de cadenas de texto de las redes sociales que cuente la entidad
  // Se realizara un metodo interno para ver a que red social pertenece y ajustar iconos
  List<String> urlderedes = [];
  // Identificador de cada entidad en base de datos interna Sutatausa App
  int idbasedatos = 0;
  double calificacion = 0.0;
  // Indica alianza con Sutatausa App donde se lleven los productos
  bool alianzaenvio = false;
  // Si la entidad no es recomendada valor = -1
  // Número de prioridad en las recomendaciones
  int recomendado = -1;
  Entidad.vacio();
  Entidad();
}
