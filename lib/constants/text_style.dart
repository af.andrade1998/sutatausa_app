import 'package:flutter/material.dart';
import 'color.dart';
import 'package:sutatausa_app/constants/color.dart';

const TextStyle tsytextobotones = TextStyle(
    fontFamily: 'poppins_regular', color: clrunselectColor, fontSize: 14);
const TextStyle tsytituloapp =
    TextStyle(fontFamily: 'lobster', fontSize: 24, color: clrwhite);
const TextStyle tsysubtituloapp =
    TextStyle(fontFamily: 'poppins_regular', color: clrwhite, fontSize: 14);
const TextStyle tsytitulosappbar =
    TextStyle(fontFamily: 'poppins_regular', color: clrwhite, fontSize: 18);
const TextStyle tsybarrabusqueda = TextStyle(
    fontFamily: 'poppins_regular', color: clrbarrabusquedaicono, fontSize: 14);
const TextStyle tsybarrabusqueda2 =
    TextStyle(fontFamily: 'poppins_regular', color: clrtextoouno, fontSize: 14);
const TextStyle tsytitulosuno =
    TextStyle(fontFamily: 'poppins_medium', color: clrtextoouno, fontSize: 26);
const TextStyle tsytitulosbotonecategorias =
    TextStyle(fontFamily: 'poppins_regular', color: clrtextoouno, fontSize: 14);
const TextStyle tsysubtitulosuno =
    TextStyle(fontFamily: 'poppins_regular', color: clrtextoouno, fontSize: 14);

const TextStyle tsytarjetaproductotitulo =
    TextStyle(fontFamily: 'poppins_regular', color: clrtextoouno, fontSize: 14);
const TextStyle tsytarjetaproductosubtitulo =
    TextStyle(fontFamily: 'poppins_light', color: clrtextoouno, fontSize: 14);
const TextStyle tsytarjetaproductodescuento =
    TextStyle(fontFamily: 'poppins_light', color: Colors.red, fontSize: 14);

const TextStyle tsytabbaritemselec =
    TextStyle(fontFamily: 'poppins_regular', color: clrtextoouno, fontSize: 14);
const TextStyle tsytabbaritemunselec =
    TextStyle(fontFamily: 'poppins_regular', color: clrtextoouno, fontSize: 14);
const TextStyle tsytabbarviewtitulo =
    TextStyle(fontFamily: 'poppins_regular', color: clrtextoouno, fontSize: 22);
const TextStyle tsytitulotarjetalarga =
    TextStyle(fontFamily: 'poppins_regular', color: clrtextoouno, fontSize: 16);
const TextStyle tsytarjetavacia =
    TextStyle(fontFamily: 'poppins_regular', color: clrtextoouno, fontSize: 16);
const TextStyle tsytarjetaentidadnegrilla =
    TextStyle(fontFamily: 'poppins_medium', color: clrtextoouno, fontSize: 16);
const TextStyle tsytarjetaentidadtexto =
    TextStyle(fontFamily: 'poppins_regular', color: clrtextoouno, fontSize: 16);
