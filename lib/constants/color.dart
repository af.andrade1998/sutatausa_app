import 'package:flutter/cupertino.dart';

const Color clrunselectColor = Color(0xFF959FBA);
const Color clrselectColor = Color(0xFFAD233B);
const Color clrwhite = Color(0xFFFFFFFF);
const Color clrbasecolor = Color(0xFFAD233B);
const Color clrfondo = Color(0xFFFBFBFB);
const Color clrbarrabusqueda = Color(0xFFEEEEEE);
const Color clrbarrabusquedaicono = Color(0xFFB1B7C2);
const Color clrtextoouno = Color(0xFF242833);

const Color clrcategoriauno = Color(0xFF6CA6DA);
const Color clrcategoriados = Color(0xFF9669AD);
const Color clrcategoriatres = Color(0xFFF4CD54);
const Color clrcategoriacuatro = Color(0xFF96CA58);
const Color clrcategoricinco = Color(0xFF466C8E);
const Color clrcategoriaseis = Color(0xFF9E9BCC);
