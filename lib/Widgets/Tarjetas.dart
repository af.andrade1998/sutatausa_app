// ignore_for_file: must_be_immutable
import 'package:flutter/material.dart';
import 'package:sutatausa_app/Interfaz/Paginas/PaginaEntidad.dart';
import 'package:sutatausa_app/Interfaz/Paginas/VisorEntidades.dart';
import 'package:sutatausa_app/constants/color.dart';
import 'package:sutatausa_app/constants/text_style.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class ItemDivisor extends StatelessWidget {
  String titulo;
  Alignment alignmentp;
  TextStyle estilop;
  ItemDivisor(this.titulo, this.alignmentp, this.estilop, {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        alignment: alignmentp,
        padding: const EdgeInsets.only(left: 12, top: 20, right: 12),
        child: Text(titulo, style: estilop));
  }
}

class ItemDivisor2 extends StatelessWidget {
  String titulo;
  Alignment alignmentp;
  TextStyle estilop;
  ItemDivisor2(this.titulo, this.alignmentp, this.estilop, {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        alignment: alignmentp,
        padding: const EdgeInsets.only(left: 12, top: 10, right: 12),
        child: Text(titulo, style: estilop));
  }
}

class ItemDivisorSubtitulo extends StatelessWidget {
  String titulo, subtitulo;
  Alignment alignmentp;
  TextStyle estilop;
  TextStyle estilop2;
  ItemDivisorSubtitulo(
      this.titulo, this.subtitulo, this.alignmentp, this.estilop, this.estilop2,
      {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        alignment: alignmentp,
        padding: const EdgeInsets.only(left: 0, top: 20, right: 12),
        child: ListTile(
          title: Text(titulo, style: estilop),
          subtitle: Text(subtitulo, style: estilop2),
        ));
  }
}

class TarjetaCategoria extends StatelessWidget {
  String titulo;
  Color colorp;
  IconData iconopuno;
  TarjetaCategoria(this.titulo, this.colorp, this.iconopuno, {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const VisorEntidades()),
            );
          },
          child: Container(
            height: 80,
            width: 80,
            margin: const EdgeInsets.only(right: 8),
            decoration: BoxDecoration(
              color: colorp,
              borderRadius: const BorderRadius.all(Radius.circular(500)),
            ),
            child: Center(
                child: Icon(
              iconopuno,
              size: 28,
              color: clrwhite,
            )),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(top: 10),
          width: 100,
          alignment: Alignment.bottomCenter,
          child: Text(
            titulo,
            maxLines: 1,
            textAlign: TextAlign.center,
            style: tsytitulosbotonecategorias,
          ),
        ),
      ],
    );
  }
}

class TarjetaNegocioRecomendado1 extends StatelessWidget {
  String urlimagenp;
  TarjetaNegocioRecomendado1(this.urlimagenp, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
            height: 100,
            width: 100,
            margin: const EdgeInsets.only(right: 8),
            child: ClipRRect(
              borderRadius: const BorderRadius.all(Radius.circular(20)),
              child: Image.asset(
                urlimagenp,
              ),
            )),
      ],
    );
  }
}

class TarjetaNegocioRecomendado extends StatelessWidget {
  String urlimagenp;
  String textonombre;
  String textocategorias;

  TarjetaNegocioRecomendado(
      this.textonombre, this.textocategorias, this.urlimagenp,
      {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Column(
          children: [
            Container(
              height: 170,
              decoration: BoxDecoration(
                color: clrwhite,
                borderRadius: BorderRadius.circular(20),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.12),
                    spreadRadius: 1,
                    blurRadius: 2,
                    offset: const Offset(0, 2), // changes position of shadow
                  ),
                ],
              ),
              child: Column(
                children: [
                  Container(
                      height: 100,
                      width: 100,
                      margin: const EdgeInsets.only(top: 10),
                      child: ClipRRect(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(20)),
                        child: Image.asset(
                          urlimagenp,
                        ),
                      )),
                  Container(
                    margin: const EdgeInsets.only(top: 10, left: 10, right: 10),
                    width: 100,
                    alignment: Alignment.bottomCenter,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Text(
                            textonombre,
                            maxLines: 1,
                            textAlign: TextAlign.left,
                            style: tsytarjetaproductotitulo,
                          ),
                        ),
                        Container(
                          width: 100,
                          alignment: Alignment.centerLeft,
                          child: RatingBar.builder(
                              itemSize: 16,
                              unratedColor: clrbarrabusquedaicono,
                              initialRating: 5,
                              ignoreGestures: true,
                              allowHalfRating: true,
                              itemBuilder: (context, _) => const Icon(
                                    Icons.star,
                                    color: Colors.amber,
                                  ),
                              onRatingUpdate: (rating) {}),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 4,
            )
          ],
        ),
        Container(
          width: 10,
        )
      ],
    );
  }
}

class TarjetaPromoPrincipal extends StatelessWidget {
  String textonombre;
  String textoprecio;
  String textodescuento;
  String urlimagenp;
  TarjetaPromoPrincipal(
      this.textonombre, this.textoprecio, this.textodescuento, this.urlimagenp,
      {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Column(
          children: [
            Container(
              height: 170,
              decoration: BoxDecoration(
                color: clrwhite,
                borderRadius: BorderRadius.circular(20),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.12),
                    spreadRadius: 1,
                    blurRadius: 2,
                    offset: const Offset(0, 2), // changes position of shadow
                  ),
                ],
              ),
              child: Column(
                children: [
                  Container(
                      height: 100,
                      width: 100,
                      margin: const EdgeInsets.only(top: 10),
                      child: ClipRRect(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(20)),
                        child: Image.asset(
                          urlimagenp,
                        ),
                      )),
                  Container(
                    margin: const EdgeInsets.only(top: 10, left: 10, right: 10),
                    width: 100,
                    alignment: Alignment.bottomCenter,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Text(
                            textonombre,
                            maxLines: 1,
                            textAlign: TextAlign.left,
                            style: tsytarjetaproductotitulo,
                          ),
                        ),
                        SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            children: [
                              Text(
                                textoprecio,
                                maxLines: 1,
                                textAlign: TextAlign.left,
                                style: tsytarjetaproductosubtitulo,
                              ),
                              Text(
                                ' ' + textodescuento,
                                maxLines: 1,
                                textAlign: TextAlign.right,
                                style: tsytarjetaproductodescuento,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 4,
            )
          ],
        ),
        Container(
          width: 10,
        )
      ],
    );
  }
}

class TabViewEntidades extends StatelessWidget {
  String titulo;
  TabViewEntidades(this.titulo, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(left: 12, top: 12, right: 12),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              titulo,
              style: tsytabbarviewtitulo,
            ),
            Expanded(
              child: ListView(
                children: [
                  TarjetaNegocioListView(
                      'Nombre negocio',
                      4,
                      'Entretenimiento - Emprendimiento - Librerías',
                      'assets/imagenes/milibro_perfil.jpg'),
                  TarjetaNegocioListView(
                      'Mi libro favorito',
                      5,
                      'Entretenimiento - Emprendimiento - Librerías',
                      'assets/imagenes/milibro_perfil.jpg'),
                  TarjetaNegocioListView(
                      'Nombre negocio',
                      4,
                      'Entretenimiento - Emprendimiento - Librerías',
                      'assets/imagenes/milibro_perfil.jpg'),
                  TarjetaNegocioListView(
                      'Mi libro favorito',
                      5,
                      'Entretenimiento - Emprendimiento - Librerías',
                      'assets/imagenes/milibro_perfil.jpg'),
                  TarjetaNegocioListView(
                      'Nombre negocio',
                      4,
                      'Entretenimiento - Emprendimiento - Librerías',
                      'assets/imagenes/milibro_perfil.jpg'),
                  TarjetaNegocioListView(
                      'Mi libro favorito',
                      5,
                      'Entretenimiento - Emprendimiento - Librerías',
                      'assets/imagenes/milibro_perfil.jpg'),
                ],
              ),
            )
          ],
        ));
  }
}

class TarjetaNegocioListView extends StatelessWidget {
  String urlimagenp;
  double numeroestrellas;
  String textonombre;
  String textocategorias;

  TarjetaNegocioListView(this.textonombre, this.numeroestrellas,
      this.textocategorias, this.urlimagenp,
      {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const PaginaEntidad()),
        );
      },
      child: Container(
        margin: const EdgeInsets.only(
          top: 10,
        ),
        decoration: BoxDecoration(
          color: clrwhite,
          borderRadius: BorderRadius.circular(20),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.12),
              spreadRadius: 1,
              blurRadius: 2,
              offset: const Offset(0, 2), // changes position of shadow
            ),
          ],
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
                height: 80,
                width: 80,
                margin: const EdgeInsets.only(top: 10, left: 10, bottom: 10),
                child: ClipRRect(
                  borderRadius: const BorderRadius.all(Radius.circular(20)),
                  child: Image.asset(
                    urlimagenp,
                  ),
                )),
            Container(
              width: 220,
              margin: const EdgeInsets.only(top: 10, left: 10, right: 10),
              alignment: Alignment.bottomCenter,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Text(
                      textonombre,
                      maxLines: 1,
                      textAlign: TextAlign.left,
                      style: tsytitulotarjetalarga,
                    ),
                  ),
                  Container(
                    width: 100,
                    alignment: Alignment.centerLeft,
                    child: RatingBar.builder(
                        itemSize: 18,
                        unratedColor: clrbarrabusquedaicono,
                        initialRating: numeroestrellas,
                        ignoreGestures: true,
                        allowHalfRating: true,
                        itemBuilder: (context, _) => const Icon(
                              Icons.star,
                              color: Colors.amber,
                            ),
                        onRatingUpdate: (rating) {}),
                  ),
                  Container(height: 6),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Text(
                      textocategorias,
                      maxLines: 1,
                      textAlign: TextAlign.left,
                      style: tsytarjetaproductotitulo,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class TarjetaListViewVacia extends StatelessWidget {
  const TarjetaListViewVacia({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 20, left: 20, right: 20),
      alignment: Alignment.topLeft,
      child: const Expanded(
        child: Text(
          "Lo sentimos aún no tenemos nada acá. Uneté a Sutatausa App.",
          textAlign: TextAlign.left,
          style: tsytarjetavacia,
        ),
      ),
    );
  }
}
