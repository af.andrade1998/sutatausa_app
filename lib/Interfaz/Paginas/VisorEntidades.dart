import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:sutatausa_app/Widgets/Tarjetas.dart';
import 'package:sutatausa_app/constants/color.dart';
import 'package:sutatausa_app/constants/text_style.dart';

class VisorEntidades extends StatefulWidget {
  const VisorEntidades({Key? key}) : super(key: key);

  @override
  State<VisorEntidades> createState() => _VisorEntidades();
}

class _VisorEntidades extends State<VisorEntidades>
    with TickerProviderStateMixin {
  double altobanner = 0;
  int activeIndex = 0;
  late TabController tabcontrol;

  @override
  void initState() {
    super.initState();
    tabcontrol = TabController(length: 3, vsync: this);
  }

  void calcularaltobanner() {
    double respuesta = 0.0;
    double proporcion = 1020 / 530;
    respuesta = (MediaQuery.of(context).size.width) / proporcion;
    setState(() {
      altobanner = respuesta;
    });
  }

  @override
  Widget build(BuildContext context) {
    calcularaltobanner();

    return Scaffold(
      backgroundColor: clrfondo,
      appBar: AppBar(
        backgroundColor: clrbasecolor,
        leading: IconButton(
          icon: const Icon(
            FontAwesomeIcons.angleLeft,
            size: 18,
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        title: const Text(
          'Comida',
          style: tsytitulosappbar,
        ),
        actions: [
          IconButton(
            onPressed: () {},
            icon: SvgPicture.asset(
              'assets/svg_files/Search.svg',
              color: clrwhite,
              height: 18,
            ),
          ),
          IconButton(
            onPressed: () {},
            icon: SvgPicture.asset(
              'assets/svg_files/Cart.svg',
              color: clrwhite,
              height: 28,
            ),
          )
        ],
      ),
      body: SafeArea(
          child: Column(
        children: [
          // Publicidad Uno
          Stack(children: [
            CarouselSlider.builder(
              options: CarouselOptions(
                  height: altobanner,
                  autoPlay: true,
                  viewportFraction: 1,
                  onPageChanged: (index, reason) {
                    setState(() {
                      activeIndex = index;
                    });
                  }),
              itemCount: anuncioUnoImagenes().length,
              itemBuilder: (context, index, realIndex) {
                String imagenactual = anuncioUnoImagenes()[index];
                return buildImage(imagenactual, index);
              },
            ),
            Center(
              child: Transform.translate(
                offset: Offset(0, altobanner - 20),
                child: AnimatedSmoothIndicator(
                  activeIndex: activeIndex,
                  count: anuncioUnoImagenes().length,
                  effect: WormEffect(
                      dotHeight: 8,
                      dotWidth: 8,
                      activeDotColor: clrwhite.withAlpha(160),
                      dotColor: clrbarrabusqueda.withAlpha(40)),
                ),
              ),
            )
          ]),
          Align(
            alignment: Alignment.centerLeft,
            child: TabBar(
                controller: tabcontrol,
                labelColor: clrtextoouno,
                isScrollable: true,
                labelPadding: const EdgeInsets.only(left: 20, right: 20),
                labelStyle: tsytabbaritemselec,
                unselectedLabelStyle: tsytabbaritemunselec,
                indicatorColor: clrbasecolor,
                indicatorWeight: 2.2,
                indicatorSize: TabBarIndicatorSize.label,
                unselectedLabelColor: clrbarrabusquedaicono,
                // ignore: prefer_const_literals_to_create_immutables
                tabs: [
                  const Tab(
                    text: 'Recomendados',
                  ),
                  const Tab(
                    text: 'Restaurantes',
                  ),
                  const Tab(
                    text: 'Comida Rápida',
                  ),
                ]),
          ),
          Expanded(
            child: TabBarView(controller: tabcontrol, children: [
              TabViewEntidades('Recomendados'),
              TabViewEntidades('Restaurantes'),
              const TarjetaListViewVacia(),
            ]),
          )
        ],
      )),
    );
  }

  List anuncioUnoImagenes() {
    List listaimagenes = [];
    listaimagenes.add('assets/imagenes/banerapp.png');
    listaimagenes.add('assets/imagenes/banerapp.png');
    return listaimagenes;
  }

  Widget buildImage(String imagep, int indexp) {
    return Image.asset(
      imagep,
      fit: BoxFit.cover,
    );
  }
}
