import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:sutatausa_app/Widgets/Tarjetas.dart';
import 'package:sutatausa_app/constants/color.dart';
import 'package:sutatausa_app/constants/text_style.dart';

class PaginaEntidad extends StatefulWidget {
  const PaginaEntidad({Key? key}) : super(key: key);

  @override
  State<PaginaEntidad> createState() => _PaginaEntidad();
}

class _PaginaEntidad extends State<PaginaEntidad>
    with TickerProviderStateMixin {
  double altobanner = 0;
  late TabController tabcontrol;

  @override
  void initState() {
    super.initState();
    tabcontrol = TabController(length: 2, vsync: this);
  }

  void calcularaltobanner() {
    double respuesta = 0.0;
    double proporcion = 1020 / 530;
    respuesta = (MediaQuery.of(context).size.width) / proporcion;
    setState(() {
      altobanner = respuesta;
    });
  }

  @override
  Widget build(BuildContext context) {
    calcularaltobanner();

    return Scaffold(
      backgroundColor: clrfondo,
      appBar: AppBar(
        backgroundColor: clrbasecolor,
        leading: IconButton(
          icon: const Icon(
            FontAwesomeIcons.angleLeft,
            size: 18,
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        title: const Text(
          'Comida',
          style: tsytitulosappbar,
        ),
        actions: [
          IconButton(
            onPressed: () {},
            icon: SvgPicture.asset(
              'assets/svg_files/Search.svg',
              color: clrwhite,
              height: 18,
            ),
          ),
          IconButton(
            onPressed: () {},
            icon: SvgPicture.asset(
              'assets/svg_files/Cart.svg',
              color: clrwhite,
              height: 28,
            ),
          )
        ],
      ),
      body: SafeArea(
          child: Column(
        children: [
          // Publicidad Uno
          Stack(children: [
            const Image(
              image: AssetImage('assets/imagenes/cafejuancho_portada.png'),
            ),
            Center(
              child: Transform.translate(
                offset: const Offset(0, 70),
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.circle,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.12),
                        spreadRadius: 1,
                        blurRadius: 2,
                        offset: const Offset(0, 2),
                      )
                    ],
                  ),
                  child: const CircleAvatar(
                    backgroundImage:
                        AssetImage('assets/imagenes/milibro_perfil.jpg'),
                    radius: 60,
                  ),
                ),
              ),
            ),
          ]),
          Container(
            height: 40,
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: TabBar(
                controller: tabcontrol,
                labelColor: clrtextoouno,
                isScrollable: true,
                labelPadding: const EdgeInsets.only(left: 20, right: 20),
                labelStyle: tsytabbaritemselec,
                unselectedLabelStyle: tsytabbaritemunselec,
                indicatorColor: clrbasecolor,
                indicatorWeight: 2.2,
                indicatorSize: TabBarIndicatorSize.label,
                unselectedLabelColor: clrbarrabusquedaicono,
                // ignore: prefer_const_literals_to_create_immutables
                tabs: [
                  const Tab(
                    text: 'Información',
                  ),
                  const Tab(
                    text: 'Productos',
                  ),
                ]),
          ),
          Expanded(
            child: TabBarView(controller: tabcontrol, children: [
              Container(
                margin: const EdgeInsets.only(
                  top: 10,
                  left: 10,
                  right: 10,
                  bottom: 20,
                ),
                decoration: BoxDecoration(
                  color: clrwhite,
                  borderRadius: BorderRadius.circular(20),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.12),
                      spreadRadius: 1,
                      blurRadius: 2,
                      offset: const Offset(0, 2), // changes position of shadow
                    ),
                  ],
                ),
                child: ListView(
                  padding: const EdgeInsets.only(
                      top: 20, left: 20, right: 20, bottom: 20),
                  children: [
                    Row(
                      // ignore: prefer_const_literals_to_create_immutables
                      children: [
                        // ignore: prefer_const_constructors
                        Text(
                          "Nombre: ",
                          style: tsytarjetaentidadnegrilla,
                        ),
                        // ignore: prefer_const_constructors
                        Text(
                          "Mi libro favorito",
                          style: tsytarjetaentidadtexto,
                        )
                      ],
                    ),
                    const Divider(
                      height: 20,
                      color: clrunselectColor,
                    ),
                    Row(
                      // ignore: prefer_const_literals_to_create_immutables
                      children: [
                        // ignore: prefer_const_constructors
                        Text(
                          "Nombre: ",
                          style: tsytarjetaentidadnegrilla,
                        ),
                        // ignore: prefer_const_constructors
                        Text(
                          "Mi libro favorito",
                          style: tsytarjetaentidadtexto,
                        )
                      ],
                    ),
                  ],
                ),
              ),
              TabViewEntidades('Restaurantes'),
            ]),
          )
        ],
      )),
    );
  }

  List anuncioUnoImagenes() {
    List listaimagenes = [];
    listaimagenes.add('assets/imagenes/banerapp.png');
    listaimagenes.add('assets/imagenes/banerapp.png');
    return listaimagenes;
  }

  Widget buildImage(String imagep, int indexp) {
    return Image.asset(
      imagep,
      fit: BoxFit.cover,
    );
  }
}
