// ignore: file_names
import 'package:flutter/material.dart';
import 'package:sutatausa_app/Interfaz/Paginas/Carrito.dart';
import 'package:sutatausa_app/Interfaz/Paginas/Guia.dart';
import 'package:sutatausa_app/Interfaz/Paginas/Perfil.dart';
import 'package:sutatausa_app/Interfaz/Paginas/Prensa.dart';
import 'package:sutatausa_app/Interfaz/Paginas/Principal.dart';
import 'package:sutatausa_app/constants/color.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sutatausa_app/constants/text_style.dart';

class Inicio extends StatefulWidget {
  const Inicio({Key? key}) : super(key: key);
  @override
  LoginState createState() => LoginState();
}

class LoginState extends State<Inicio> {
  int current = 0;
  PageController controlpaginas = PageController(initialPage: 0);

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: clrfondo,
        bottomNavigationBar: Container(
          decoration: BoxDecoration(
            boxShadow: <BoxShadow>[
              BoxShadow(
                color: Colors.grey.withOpacity(0.2),
                spreadRadius: 4,
                blurRadius: 12,
              ),
            ],
          ),
          height: MediaQuery.of(context).size.height * 0.1,
          child: BottomNavigationBar(
            elevation: 0,
            type: BottomNavigationBarType.shifting,
            backgroundColor: Colors.white,
            unselectedItemColor: clrunselectColor,
            selectedItemColor: clrselectColor,
            selectedLabelStyle: tsytextobotones,
            currentIndex: current,
            items: navBarItems(),
            onTap: (index) {
              setState(() {
                current = index;
                controlpaginas.animateToPage(index,
                    duration: const Duration(milliseconds: 500),
                    curve: Curves.ease);
              });
            },
          ),
        ),
        //body:  listaTabs()[current]);
        body: PageView(
          controller: controlpaginas,
          onPageChanged: (newIndex) {
            setState(() {
              current = newIndex;
            });
          },
          children: [
            listaTabs()[0],
            listaTabs()[1],
            listaTabs()[2],
            listaTabs()[3],
            listaTabs()[4],
          ],
        ));
  }

  List<BottomNavigationBarItem> navBarItems() {
    List<BottomNavigationBarItem> respuesta = [];
    respuesta.add(BottomNavigationBarItem(
      icon: SvgPicture.asset(
        'assets/svg_files/Home_6.svg',
        color: current == 0 ? clrselectColor : clrunselectColor,
      ),
      label: 'Inicio',
      backgroundColor: Colors.white,
    ));

    respuesta.add(BottomNavigationBarItem(
      icon: SvgPicture.asset(
        'assets/svg_files/map_paper.svg',
        color: current == 1 ? clrselectColor : clrunselectColor,
      ),
      label: 'Guía',
      backgroundColor: Colors.white,
    ));

    respuesta.add(BottomNavigationBarItem(
      icon: SvgPicture.asset('assets/svg_files/Dairy_2.svg',
          color: current == 2 ? clrselectColor : clrunselectColor),
      label: 'Prensa',
      backgroundColor: Colors.white,
    ));

    respuesta.add(BottomNavigationBarItem(
      icon: SvgPicture.asset('assets/svg_files/Cart.svg',
          color: current == 3 ? clrselectColor : clrunselectColor),
      label: 'Carrito',
      backgroundColor: Colors.white,
    ));
    respuesta.add(BottomNavigationBarItem(
      icon: SvgPicture.asset('assets/svg_files/User_4.svg',
          color: current == 4 ? clrselectColor : clrunselectColor),
      label: 'Perfil',
      backgroundColor: Colors.white,
    ));
    return respuesta;
  }

  List<Widget> listaTabs() {
    List<Widget> respuesta = [];

    respuesta.add(const Center(child: Principal()));
    respuesta.add(const Center(child: Guia()));
    respuesta.add(const Center(child: Prensa()));
    respuesta.add(const Center(child: Carrito()));
    respuesta.add(const Center(child: Perfil()));

    return respuesta;
  }
}
