// ignore: file_names
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sutatausa_app/Widgets/Tarjetas.dart';
import 'package:sutatausa_app/constants/color.dart';
import 'package:sutatausa_app/constants/text_style.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class Principal extends StatefulWidget {
  const Principal({Key? key}) : super(key: key);

  @override
  State<Principal> createState() => _Principal();
}

class _Principal extends State<Principal> {
  final TextEditingController controlbusqueda = TextEditingController();
  double altobanner = 0;

  List botonesCategorias = [
    TarjetaCategoria(
      'Comida',
      clrcategoriauno,
      FontAwesomeIcons.pizzaSlice,
    ),
    TarjetaCategoria(
      'Supermercado',
      clrcategoriados,
      FontAwesomeIcons.basketShopping,
    ),
    TarjetaCategoria(
      'Compras',
      clrcategoriatres,
      FontAwesomeIcons.store,
    ),
    TarjetaCategoria(
      'Servicios',
      clrcategoriacuatro,
      FontAwesomeIcons.taxi,
    ),
    TarjetaCategoria(
      'Entretenimiento',
      clrcategoricinco,
      FontAwesomeIcons.gamepad,
    ),
    TarjetaCategoria(
      'Turismo',
      clrcategoriaseis,
      FontAwesomeIcons.hotel,
    ),
    TarjetaCategoria(
      'Emprendimiento',
      clrcategoriatres,
      FontAwesomeIcons.lightbulb,
    ),
    TarjetaCategoria('Campo', clrcategoriacuatro, FontAwesomeIcons.carrot),
    TarjetaCategoria(
      'Farmacias',
      clrcategoriauno,
      FontAwesomeIcons.capsules,
    ),
  ];

  void calcularaltobanner() {
    double respuesta = 0.0;
    double proporcion = 1020 / 286;
    respuesta = (MediaQuery.of(context).size.width - 24) / proporcion;
    setState(() {
      altobanner = respuesta;
    });
  }

  @override
  Widget build(BuildContext context) {
    calcularaltobanner();
    return ListView(
      children: [
        // Tarjeta Presentación
        Container(
          width: MediaQuery.of(context).size.width,
          height: 80,
          decoration: const BoxDecoration(
            color: clrbasecolor,
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(14),
                bottomRight: Radius.circular(14)),
          ),
          child: Center(
            child: ListTile(
              title: const Text("Sutatausa App", style: tsytituloapp),
              subtitle: const Text(
                "¡Bienvenido! Todo en un solo lugar",
                style: tsysubtituloapp,
              ),
              trailing: Transform.scale(
                scale: 1.4,
                origin: const Offset(20, 0),
                child: SvgPicture.asset(
                  'assets/svg_files/mono.svg',
                  color: clrwhite,
                ),
              ),
            ),
          ),
        ),
        // Barra de buscar
        Container(
          margin: const EdgeInsets.only(top: 20, left: 12, right: 12),
          height: 54,
          decoration: const BoxDecoration(
            color: clrbarrabusqueda,
            borderRadius: BorderRadius.all(Radius.circular(20)),
          ),
          child: Center(
              child: ListTile(
                  leading: Transform.translate(
                    offset: const Offset(10, 0),
                    child: SvgPicture.asset(
                      'assets/svg_files/Search.svg',
                      color: clrbarrabusquedaicono,
                    ),
                  ),
                  title: Transform.translate(
                      offset: const Offset(-10, 0),
                      child: TextField(
                        textAlign: TextAlign.left,
                        controller: controlbusqueda,
                        style: tsybarrabusqueda2,
                        decoration: const InputDecoration.collapsed(
                          hintText: 'Encuentra todo lo que quieres...',
                          hintStyle: tsybarrabusqueda,
                        ),
                      )),
                  trailing: Transform.translate(
                    offset: const Offset(-10, 0),
                    child: SvgPicture.asset(
                      'assets/svg_files/Cancel.svg',
                    ),
                  ))),
        ),
        // Titulo Categorias
        ItemDivisor('Categorías', Alignment.centerLeft, tsytitulosuno),
        // Lista de categorias

        Column(
          children: [
            Container(
                margin: const EdgeInsets.only(top: 10, left: 12, right: 12),
                height: 118,
                child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: botonesCategorias.length,
                    itemBuilder: (BuildContext context, int index) {
                      return botonesCategorias[index];
                    })),
          ],
        ),

        //Titulo Destacados
        ItemDivisor('Destacado', Alignment.centerLeft, tsytitulosuno),
        // Publicidad Uno
        Container(
          decoration: const BoxDecoration(
            color: Colors.black,
            borderRadius: BorderRadius.all(Radius.circular(14)),
          ),
          margin: const EdgeInsets.only(top: 10, left: 12, right: 12),
          child: ClipRRect(
            borderRadius: const BorderRadius.all(Radius.circular(14)),
            child: CarouselSlider.builder(
              options: CarouselOptions(
                height: altobanner,
                autoPlay: true,
                viewportFraction: 1,
              ),
              itemCount: anuncioUnoImagenes().length,
              itemBuilder: (context, index, realIndex) {
                String imagenactual = anuncioUnoImagenes()[index];
                return buildImage(imagenactual, index);
              },
            ),
          ),
        ),
        // Titulo negocios destacados
        ItemDivisorSubtitulo(
            'Nuestros favoritos',
            'Lo mejor de Sutatausa para tí',
            Alignment.centerLeft,
            tsytitulosuno,
            tsysubtitulosuno),
        // Lista de nuestros favoritos
        Column(
          children: [
            Container(
                margin: const EdgeInsets.only(top: 10, left: 12, right: 12),
                height: 180,
                child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: listaNegociosRecomendados().length,
                    itemBuilder: (BuildContext context, int index) {
                      return TarjetaNegocioRecomendado(
                          'Nombre negocio',
                          'Restaurantes-Emprendimiento',
                          listaNegociosRecomendados()[index]);
                    })),
          ],
        ),
        // Titulo promos
        ItemDivisorSubtitulo(
            'Productos destacados',
            'Promociones y productos increibles para tí',
            Alignment.centerLeft,
            tsytitulosuno,
            tsysubtitulosuno),
        // Lista de promos
        Column(
          children: [
            Container(
                margin: const EdgeInsets.only(top: 10, left: 12, right: 12),
                height: 174,
                child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: listaNegociosRecomendados().length,
                    itemBuilder: (BuildContext context, int index) {
                      return TarjetaPromoPrincipal(
                          'Harry Potter y la cámara secreta',
                          '\$20.000',
                          '15%',
                          'assets/imagenes/Libro3.jpg');
                    })),
          ],
        ),
        //Titulo recomendados
        ItemDivisor('Recomendados', Alignment.centerLeft, tsytitulosuno),
        // Subtitulo comida
        ItemDivisor2('Comida', Alignment.centerLeft, tsysubtitulosuno),
        // Lista de favoritos comida
        Column(
          children: [
            Container(
                margin: const EdgeInsets.only(top: 10, left: 12, right: 12),
                height: 180,
                child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: listaNegociosRecomendados().length,
                    itemBuilder: (BuildContext context, int index) {
                      return TarjetaNegocioRecomendado(
                          'Nombre negocio',
                          'Restaurantes-Emprendimiento',
                          listaNegociosRecomendados()[index]);
                    })),
          ],
        ),
        // Subtitulo compras
        ItemDivisor2('Compras', Alignment.centerLeft, tsysubtitulosuno),
        // Lista de favoritos compras
        Column(
          children: [
            Container(
                margin: const EdgeInsets.only(top: 10, left: 12, right: 12),
                height: 180,
                child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: listaNegociosRecomendados().length,
                    itemBuilder: (BuildContext context, int index) {
                      return TarjetaNegocioRecomendado(
                          'Nombre negocio',
                          'Restaurantes-Emprendimiento',
                          listaNegociosRecomendados()[index]);
                    })),
          ],
        ),
        // Subtitulo servicios
        ItemDivisor2('Servicios', Alignment.centerLeft, tsysubtitulosuno),
        // Lista de favoritos servicios
        Column(
          children: [
            Container(
                margin: const EdgeInsets.only(top: 10, left: 12, right: 12),
                height: 180,
                child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: listaNegociosRecomendados().length,
                    itemBuilder: (BuildContext context, int index) {
                      return TarjetaNegocioRecomendado(
                          'Nombre negocio',
                          'Restaurantes-Emprendimiento',
                          listaNegociosRecomendados()[index]);
                    })),
          ],
        ),
        // Subtitulo entre
        ItemDivisor2('Entretenimiento', Alignment.centerLeft, tsysubtitulosuno),
        // Lista de favoritos entre
        Column(
          children: [
            Container(
                margin: const EdgeInsets.only(top: 10, left: 12, right: 12),
                height: 180,
                child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: listaNegociosRecomendados().length,
                    itemBuilder: (BuildContext context, int index) {
                      return TarjetaNegocioRecomendado(
                          'Nombre negocio',
                          'Restaurantes-Emprendimiento',
                          listaNegociosRecomendados()[index]);
                    })),
          ],
        ),
        // Subtitulo turismo
        ItemDivisor2('Turismo', Alignment.centerLeft, tsysubtitulosuno),
        // Lista de favoritos turismo
        Column(
          children: [
            Container(
                margin: const EdgeInsets.only(top: 10, left: 12, right: 12),
                height: 180,
                child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: listaNegociosRecomendados().length,
                    itemBuilder: (BuildContext context, int index) {
                      return TarjetaNegocioRecomendado(
                          'Nombre negocio',
                          'Restaurantes-Emprendimiento',
                          listaNegociosRecomendados()[index]);
                    })),
          ],
        ),
        //Titulo Destacados
        ItemDivisor('Destacado', Alignment.centerLeft, tsytitulosuno),
        // Publicidad Uno
        Container(
          decoration: const BoxDecoration(
            color: Colors.black,
            borderRadius: BorderRadius.all(Radius.circular(14)),
          ),
          margin: const EdgeInsets.only(top: 10, left: 12, right: 12),
          child: ClipRRect(
            borderRadius: const BorderRadius.all(Radius.circular(14)),
            child: CarouselSlider.builder(
              options: CarouselOptions(
                height: altobanner,
                autoPlay: true,
                viewportFraction: 1,
              ),
              itemCount: anuncioUnoImagenes().length,
              itemBuilder: (context, index, realIndex) {
                String imagenactual = anuncioUnoImagenes()[index];
                return buildImage(imagenactual, index);
              },
            ),
          ),
        ),
        Container(
          height: 26,
        )
      ],
    );
  }

  Widget buildImage(String imagep, int indexp) {
    return Image.asset(
      imagep,
      fit: BoxFit.cover,
    );
  }

  List anuncioUnoImagenes() {
    List listaimagenes = [];
    listaimagenes.add('assets/imagenes/anunciounojuancho.png');
    listaimagenes.add('assets/imagenes/anunciounomilibrofav.png');
    return listaimagenes;
  }

  List listaNegociosRecomendados() {
    List listaimagenes = [];
    listaimagenes.add('assets/imagenes/juanchocafe_perfil.png');
    listaimagenes.add('assets/imagenes/milibro_perfil.jpg');
    listaimagenes.add('assets/imagenes/justudios_perfil.png');
    listaimagenes.add('assets/imagenes/casona_perfil.png');
    listaimagenes.add('assets/imagenes/bosqueescondido_perfil.png');

    return listaimagenes;
  }
}
